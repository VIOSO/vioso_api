#ifndef VWB_SOCKET_HPP
#define VWB_SOCKET_HPP

#include "Platform.h"
//#include "../Include/VWBTypes.h"
#include <memory>
#include <list>
#include <queue>
#include <map>
#include <string>
#include <atomic>

#if defined( WIN32 ) || defined( WIN64 )

	#define NOMINMAX
	#define INCL_EXTRA_HTON_FUNCTIONS
	#include <WS2tcpip.h>
	#include <winsock2.h>
	#undef NOMINMAX

	#pragma comment(lib, "ws2_32.lib")

	#define ERRINTR (WSAEINTR)
	#define lastNetError WSAGetLastError()
	typedef long suseconds_t;
	typedef int socklen_t;

	#define ifStartSockets() 	for( WSADATA d = {0}; 0 == d.wVersion && 0 == WSAStartup(0x0002,&d); )
	#define closeSockets WSACleanup
	#define FD_IS_ANY_SET(fd) (fd.fd_count != 0)

#else /* WIN32 */

	#include "sys/socket.h"
	#include "sys/types.h"
	#include "sys/param.h"
	#include "sys/time.h"
	#include "netinet/in.h"
	#include "sys/select.h"

	#define SOCKET int
	#define INVALID_SOCKET (SOCKET)(~0)
	#define SOCKET_ERROR (-1)
	#define ERRINTR (10004)     //can native bsd socket's be interrupted?
    #define lastNetError errno
    #define ifStartSockets() if(1)
    #define closeSockets() (0)
    typedef struct WSADATA { int i; } WSADATA;
    typedef fd_set FD_SET;
    typedef unsigned char UCHAR;
    typedef uint32_t ULONG;
    #define FD_IS_ANY_SET(fd) (fd.fds_bits != 0)
#endif /* WIN32 */

class Socket;
struct SocketAddress;
#define INFINITETIMEOUT 1.7976931348623158e+308

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
//                               SocketAddress
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

struct SocketAddress : sockaddr_in 
{
// construction

	// use default constructor to create empty address

	// copy constructor
	SocketAddress(sockaddr const& sa)
	{
		memcpy(this, &sa, sizeof(sockaddr));
	}; 

	// construct object from sockaddr_in struct
	SocketAddress(sockaddr_in const& sin)
	{
		memcpy(this, &sin, sizeof(sockaddr_in));
	}; 

	// construct AF_INET address using UINT-address in net-byte-order and port in host-byte-order.
	SocketAddress(unsigned long addr=INADDR_ANY, unsigned short port=0);

	// construct AF_INET address using url string and port number in host byte order.
	SocketAddress(char const* url, unsigned short port = 0); 

// public methods

	// returns ip in dotted decimal format i.e. "10.1.1.1".
	char const*  getDottedDecimal( char* sz, size_t nsz ) const
	{
		return inet_ntop(sin_family, (VWB_inet_ntop_cast)&sin_addr, sz, nsz);
	}; 
	template< size_t _sz >
	char const*  getDottedDecimal(char(&sz)[_sz]) const { return getDottedDecimal(sz, _sz); }

	// gets / sets port number in host byte order
	unsigned short	getPort() const;
	void			setPort(unsigned short port);

	// resets address to zero
	void zero() { memset(this,0,sizeof(SocketAddress)); }

// public operators for conversion

	operator sockaddr() const
	{
		return *((sockaddr*) this);
	}

	operator sockaddr*() const
	{
		return (sockaddr*) this;
	}

	operator sockaddr*()
	{
		return (sockaddr*) this;
	}

	operator sockaddr&() const
	{
		return *((sockaddr*) this);
	}

	operator const sockaddr&() const
	{
		return (const sockaddr&)*((sockaddr*) this);
	}

// public asignement and compare operators

	const SocketAddress& operator=(const sockaddr& sa);

	const SocketAddress& operator=(const sockaddr_in& sin);

	bool operator==(const SocketAddress& sa) const { return 0 == memcmp(this,&sa,sizeof(SocketAddress)); }

// public static methods

	// constructs a broadcast address to a specific port in host byte order.
	static SocketAddress broadcast(unsigned short port);
};

/**************** Socket *****************/

class Socket {

#ifdef WIN32

#	define ioctl IOCtl
#	define IFNAMSIZ 16

	struct ifreq 
	{
		char ifr_name[IFNAMSIZ];
		union {
			sockaddr  ifru_addr;
			sockaddr  ifru_dstaddr;
			char      ifru_oname[IFNAMSIZ];
			sockaddr  ifru_broadaddr;
			short     ifru_flags;
			int       ifru_metric;
			char      ifru_data[1];
			char      ifru_enaddr[6];
		};
	};

#endif /* WIN32 */

protected:
		
	SOCKET sock;

public:

//construction

	// construct empty socket
	Socket() { sock = 0; }

	// manage native
	Socket( SOCKET const& s ) { sock = s; }

	// construct and create socket of type i.e. SOCK_DGRAM or SOCK_STREAM
	Socket( int type/*=SOCK_STREAM*/, bool broadcast/*=FALSE*/, int protocol/*=0*/, bool keepAlive/*=FALSE*/, u_int qRecvBuffer/*=0*/, u_int qSendBuffer/*=0*/ )
	{
		if( SOCKET_ERROR == create( type, broadcast, protocol, keepAlive, qRecvBuffer, qSendBuffer ) )
			throw SOCKET_ERROR;
	};

	// copy constructor
	Socket( Socket const& ) = delete;
	Socket& operator=( Socket const& ) = delete;

	Socket( Socket&& other ) noexcept : sock( other.sock ) { other.sock = 0; }
	Socket& operator=( Socket&& other ) noexcept { std::swap( sock, other.sock ); return *this; };
	~Socket() { close(); }

// public methods

	// creates socket handle
	int create(int type/*=SOCK_STREAM*/, bool broadcast/*=FALSE*/, int protocol/*=0*/, bool keepAlive/*=FALSE*/,u_int qRecvBuffer/*=0*/,u_int qSendBuffer/*=0*/);

	// set size of receive buffer
	int setRecvBuff(int size);

	// get size of receive buffer
	int getRecvBuffSize(int& size);

	// get size of receive buffer
	int getSendBuffSize(int& size);

	// close socket
	int close(); 

	// bind socket to local address
	int bind(SocketAddress const& sa)
	{ 
		return ::bind(sock,sa,sizeof(sockaddr));
	};

	// listen on socket
	int listen(int qConnMax=0)
	{ 
		return ::listen( sock, (qConnMax)? qConnMax : SOMAXCONN);
	};

	// connect socket to peer address
	int connect(SocketAddress const& sa)
	{ 
		return ::connect(sock,sa,sizeof(sockaddr));
	};

	// accepts connections; socket needs to be stream oriented
	Socket accept()
	{ 
		return (SOCKET)::accept(sock,NULL,NULL);
	}; 

	// accepts connections; socket needs to be stream oriented
	Socket accept( SocketAddress& peerAddr )
	{ 
		socklen_t l = sizeof( SocketAddress );
		return (SOCKET)::accept(sock, (sockaddr*)&peerAddr, &l );
	}; 

	// check socket to be ready to read from, operation blocks
	int tryRead(const double timeout=INFINITETIMEOUT) const;

	// check socket to be ready to write to, operation blocks
	int tryWrite(const double timeout=INFINITETIMEOUT) const;

	// check for any exceptions on socket
	int test(const double timeout=INFINITETIMEOUT) const;

	// send some data to a conected socket, operation blocks
	int send(const char* pch, int iSize, const double timeout=INFINITETIMEOUT); 

	// send some data to a concected socket direct, without select
	int sendDirect(const char* pch, int iSize); 

	// receive some date from connected socket, operation blocks
	int recv(char* pch, int iSize, const double timeout=INFINITETIMEOUT); 

	// receive some date from connected socket direct, witout select
	int recvDirect(char* pch, int iSize); 

	// sends datagram to peer
	int sendDatagram(const char* buf, const int iSize, SocketAddress const& sa, bool dontRoute = false);
	int sendDatagram( const char* buf, SocketAddress const& sa, bool dontRoute = false ) // for zero terminated string
	{
		int sz = ( int )strnlen_s( buf, 0xFFFF );
		return sendDatagram( buf, sz, sa, dontRoute );
	} 

	// receives datagram from socket, peer address will be stored in 'sa' if specified
	int recvDatagram(char* buf, const int iSize, SocketAddress* sa=NULL, const double timeout = INFINITETIMEOUT );
	template<int sz> int recvDatagram( char( &buf )[sz], SocketAddress* sa, const double timeout = INFINITETIMEOUT ) { return recvDatagram( buf, sz, sa, timeout ); }

	// returns connected peer address
	SocketAddress getpeeraddr();

	// returns bound local address
	SocketAddress getsockaddr();
    int getsockaddr(sockaddr_in& addr,socklen_t& nAddr);

// public static methods

	// close socket
	static int close(SOCKET& s);

	// these are wrapper functions for os native operations derived from berkeley sockets
	static SocketAddress  gethostbyname(const std::string& name, const unsigned short port=0); 
	static std::string gethostname(); 
	static std::vector<in_addr> getLocalIPList(); 
	static std::string gethostbyaddr(const SocketAddress& sa); 
	static std::string getErrorMessage(int err);

	// the ntoh and hton functions are now polymorph
	static _inline_ u_short ntoh(u_short netshort) { return ::ntohs(netshort); }
	static _inline_ u_short hton(u_short hostshort) { return ::htons(hostshort); }
	static _inline_ short ntoh(short netshort) { u_short r = ::ntohs(*reinterpret_cast<u_short*>(&netshort)); return *reinterpret_cast<short*>(&r); }
	static _inline_ short hton(short hostshort) { u_short r = ::htons(*reinterpret_cast<u_short*>(&hostshort)); return *reinterpret_cast<short*>(&r); }
	static _inline_ u_long   ntoh(u_long   netint) { return ::ntohl(netint); }
	static _inline_ u_long   hton(u_long   hostint) { return ::htonl(hostint); }
	static _inline_ long   ntoh(long   netint) { u_long r = ::ntohl(*reinterpret_cast<u_long*>(&netint)); return *reinterpret_cast<long*>(&r); }
	static _inline_ long   hton(long   hostint) { u_long r = ::htonl(*reinterpret_cast<u_long*>(&hostint)); return *reinterpret_cast<long*>(&r); }
	//static _inline_ u_int64 ntoh(u_int64 netlong) { return ntohll(netlong); }
	//static _inline_ u_int64 hton(u_int64 hostlong) { return htonll(hostlong); }
	//static _inline_ int64_t ntoh(int64_t netlong) { return *(int64_t*)::ntohll(*(u_int64*)&netlong); }
	//static _inline_ int64_t hton(int64_t  hostlong) { return *(int64_t*)::htonll(*(u_int64*)&hostlong); }
	static _inline_ in_addr makeInAddr(UCHAR b1, UCHAR b2, UCHAR b3, UCHAR b4)
	{
        in_addr ret;
        ret.s_addr = uint32_t(b1) + ( uint32_t(b2) << 8 ) + ( uint32_t(b3) << 16 ) + ( uint32_t(b4) << 24 );
		return ret;
	}
	static _inline_ in_addr makeInAddr(ULONG addr)
	{
		in_addr ret;
		ret.s_addr = addr;
		return ret;
	}

	operator SOCKET() { return sock; };
	bool operator==(const Socket& s) const { return sock == s.sock; };
	bool operator==(const SOCKET s) const { return sock == s; };
	friend bool operator==(const SOCKET s, const Socket& ss) { return s == ss.sock; };
};

class Server;

class SockIn : public Socket
{
	friend Server;
public:
	SockIn() = default;
	SockIn( int protocol, SocketAddress const& sa );
	SockIn( SockIn&& ) = default;
	SockIn( Socket&& other ) : Socket( std::move(other) ) {}
	Socket detach() { Socket s(sock); sock = 0; return s; }

	virtual int cbRead( Server* pServer ) {return 0;}
	virtual int cbWrite( Server* pServer ) {return 0;}
	virtual int cbError( Server* pServer, int err ) {return err;}
protected:
	// return 0 for ok but break, positive if ok and listened
	virtual int processRead( Server* pServer ) {return 1;}
	virtual int processWrite( Server* pServer ) {return 1;}
	virtual int processError( Server* pServer ) {return 1;}
};


//////////////////////////////////////////////////////////////
// the server handles all connections. It listenes to a port, either UDP or TCP
class Server
{
public:
	typedef std::vector< std::shared_ptr<SockIn> > Listeners;
	typedef enum MODALSTATE {
		MODALSTATE_RUN = -1025,
		MODALSTATE_ERR = -2049,
		MODALSTATE_EXC = -1535
	} MODALSTATE;
protected:
	Listeners	m_listeners;
	FD_SET		m_readers;
    FD_SET		m_writers;
    std::atomic_int m_modalState;
	std::thread	m_thread;
	std::mutex	m_mtxGlobal;
public:
	timeval m_sto; // standard time-out value
	Server();
	Server( std::shared_ptr<SockIn> const& s, bool bRunModal );
	virtual ~Server();

	int addReceiver( std::shared_ptr<SockIn> s );
	int removeReceiver( std::shared_ptr<SockIn> s );

	// s needs to be in listeners already!!
	int addResponder( Socket& s );
	int removeResponder( Socket& s );
	bool empty() { return m_listeners.empty(); }
	int doModalStep(); // call in external loop if not modal
	int endModal( int code );
	int doModal();
	bool isRunningModal();
private:
	u_int modalLoop();
};


#endif //ndef VWB_SOCKET_HPP
